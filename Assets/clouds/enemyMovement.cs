﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMovement : MonoBehaviour
{
    public double MoveSpeed = 0.04;
    float x, y;
    // Start is called before the first frame update
    void Start()
    {
        x = transform.position.x;
        y = transform.position.y;

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        x = transform.position.x - (float)MoveSpeed;
        transform.position = new Vector2(x, y);
    }
}
