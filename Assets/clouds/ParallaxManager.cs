﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Parallax[] background;
    Mover mover;
    GameObject[] getcount;

    private void Start()
    {


        foreach (Parallax back in background)
        {
            back.Background.GetComponent<Mover>().StartingPosition = back.StartingPosition;
            back.Background.GetComponent<Mover>().DestroyPosition = back.DestroyPosition;
            back.Background.GetComponent<Mover>().Speed = back.Speed;
            Instantiate(back.Background,back.StartingPosition, Quaternion.identity);
            
            
            //Debug.Log("Back INIT Manager");
            back.InitTime = (Mathf.Abs(Vector2.Distance(back.StartingPosition,back.InitPosition)) / Mathf.Abs(back.Speed)) / 60;
            back.TimeZero = back.InitTime;
            




        }
        //Debug.Log("Manager Start");

    }

    // Update is called once per frame
    void Update()
    {

        foreach (Parallax back in background)
        {

            back.TimeZero += Time.deltaTime;
            if (back.TimeZero > back.InitTime)
            {


                Instantiate(back.Background, back.InitPosition, Quaternion.identity);
                back.Background.GetComponent<Mover>().Speed = back.Speed;
                back.TimeZero = 0.0f;


            }

        }

    }
}
