﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class Parallax
{
    // Start is called before the first frame update

    public GameObject Background;
    public Vector2 InitPosition;
    public Vector2 StartingPosition;
    public Vector2 DestroyPosition;
    public float TimeZero;
    public float Speed;
    public float InitTime;
}
