﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    // Start is called before the first frame update

    ParallaxManager manager;
    public Vector2 StartingPosition;
    public Vector2 DestroyPosition;
    public float Speed;
    void Start()
    {
         
        Debug.Log("Speed" + Speed);
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector2(transform.position.x + Speed, 0);
        if (transform.position.x < DestroyPosition.x)
        {
            //Debug.Log("Mover");
            Destroy(gameObject);
        }
    }
}
