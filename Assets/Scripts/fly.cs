﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class fly : MonoBehaviour
{
    public GameObject TapCanvas;
    int force = 15;
    public GameObject ExlodeFX;
    GameObject newExplode;
    float starttime = 0f;
    float waittime = 1.5f;
    int score = 0;
    float scoretime = 0f;
    public GameObject Scoretext;
    // Start is called before the first frame update
    void Start()
    {
        //this.GetComponent<SpriteRenderer>().sortingOrder = 3;
        //this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        //this.GetComponent<SpriteRenderer>().enabled = true;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        scoretime += Time.deltaTime;
        starttime += Time.deltaTime;
        if(scoretime>2.0f)
        {
            try
            {
                scoretime = 0f;
                Scoretext.GetComponent<Text>().text = ((int.Parse(Scoretext.GetComponent<Text>().text)) + 1).ToString();
            }
            catch { }


        }
        if (Input.GetKeyDown("space") )
        {
            
            if(SceneManager.GetActiveScene().buildIndex==1 && (waittime < starttime))
            {
                starttime = 0f;
                // Debug.Log("got activescene");
                SceneManager.UnloadSceneAsync(1);
                SceneManager.LoadScene("SampleScene");
                //SceneManager.sceneLoaded += OnSceneLoaded;
                Invoke("Canvas(true)", 1f);

                
            }

            else 
            {

                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, force);

                // SetDefaultMoveSpeed();


            }

        }
        if (Input.touchCount == 1)
        {
           
            if (SceneManager.GetActiveScene().buildIndex == 1 && (waittime < starttime))
            {
                starttime = 0f;

                // Debug.Log("got activescene");
                SceneManager.UnloadSceneAsync(1);
                SceneManager.LoadScene("SampleScene");
                //SceneManager.sceneLoaded += OnSceneLoaded;

            }

            else 
            {

                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, force);

                // SetDefaultMoveSpeed();


            }

        }
       

    }
    private void Update()
    {
        

        if (Time.timeScale==0 && (Input.GetKeyDown("space") || (Input.touchCount == 1 )) && (waittime < starttime))
        {
            starttime = 0f;
            Time.timeScale = 1;
            SceneManager.UnloadSceneAsync(0);
            SceneManager.LoadScene("SS");
        }
    }
    //void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    //{
    //    if (SceneManager.GetActiveScene().buildIndex == 1)
    //    {
    //        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(0));
    //    }
    //    else
    //    {
    //        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
    //    }
    //    //SceneManager.UnloadScene(1);
    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("cloud"))
        {
            Debug.Log("Hit Enemy");
            try
            {
                newExplode = Instantiate(ExlodeFX, transform.position, Quaternion.identity);
                Destroy(newExplode, 2f);
                ExlodeFX.GetComponent<ParticleSystem>().Play();
            }
            catch
            {

            }

            //this.transform.position = new Vector2(-13, 6);

            //Invoke("StopGame", 1.5f);
            StopGame();
        this.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        this.GetComponent<SpriteRenderer>().enabled = false;







            //scoreText.text = "0";
            //Debug.Log("HIT ENEMY");
            //DeadCanvas.SetActive(true);
            //FindObjectOfType<AudioManager>().SetVolume("BG", 0.2f);
            //FindObjectOfType<AudioManager>().SetVolume("Wind", 0.0f);
            //FindObjectOfType<AudioManager>().play("Die");
            //    //dead = true;
        }
    }
    void StopGame()
    {
       
        TapCanvas.SetActive(true);
        Time.timeScale = 0;

        admobdemo.Instance.RemoveRelativeBannerAd();
        admobdemo.Instance.ShowInterstitialAd();
       

    }
    void Canvas(bool YN)
    {
        TapCanvas.SetActive(YN);
        
    }

}
 
