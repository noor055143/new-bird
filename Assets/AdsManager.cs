﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using admob;

//public class AdsManager : MonoBehaviour
//{
//    private static AdsManager instance;

//    public static AdsManager Instance
//    {
//        get
//        {
//            if (instance == null)
//            {
//                instance = GameObject.FindObjectOfType<AdsManager>();
//            }
//            return instance;
//        }
//    }
//    Admob ad;
//    string AppID = "";
//    string BannerID = "";
//    string InterstitialID = "";

//    void Awake()
//    {
//        //Test IDs
//        AppID = "ca-app-pub-3940256099942544~3347511713";
//        BannerID = "ca-app-pub-3940256099942544/6300978111";
//        InterstitialID = "ca-app-pub-3940256099942544/1033173712";

//        //Real IDs
//        //AppID = "ca-app-pub-6590923007672971~2750910329";
//        //BannerID = "ca-app-pub-6590923007672971/7664325652";
//        //InterstitialID = "ca-app-pub-6590923007672971/8288525071";
//        AdProperties adProperties = new AdProperties();
//        adProperties.isTesting = true;

//        ad = Admob.Instance();
//        ad.bannerEventHandler += onBannerEvent;
//        ad.interstitialEventHandler += onInterstitialEvent;

//        ad.initSDK(AppID, adProperties);//reqired,adProperties can been null
//    }

//    public void ShowBannerAd()
//    {
//        Admob.Instance().showBannerRelative(BannerID, AdSize.SMART_BANNER, AdPosition.BOTTOM_CENTER);

//    }

//    public void RemoveBannerAd()
//    {
//        Admob.Instance().removeBanner();
//    }

//    public void ShowInterstitialAd()
//    {
//        if (ad.isInterstitialReady())
//        {
//            ad.showInterstitial();
//        }
//        else
//        {
//            ad.loadInterstitial(InterstitialID);
//        }
//    }

//    void onInterstitialEvent(string eventName, string msg)
//    {
//        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
//        if (eventName == AdmobEvent.onAdLoaded)
//        {
//            Admob.Instance().showInterstitial();
//        }
//    }
//    void onBannerEvent(string eventName, string msg)
//    {
//        Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
//    }

//}
