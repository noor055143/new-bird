﻿using UnityEngine;
using admob;
public class admobdemo : MonoBehaviour {


    private static admobdemo instance;

    public static admobdemo Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<admobdemo>();
            }
            return instance;
        }
    }

    Admob ad;
	string appID="";
	string bannerID="";
	string interstitialID="";
	string videoID="";
	
    void Awake()
    {
        Debug.Log("Awake is called!----------");
        initAdmob();
    }

    void Start () {
        Debug.Log("start unity demo-------------");
        ShowRelativeBannerAd();
	}
	
	void Update () {
	    if (Input.GetKeyUp (KeyCode.Escape)) {
            Debug.Log(KeyCode.Escape+"-----------------");
	    }
    }
   
    void initAdmob()
    {
#if UNITY_IOS
        		// appID="ca-app-pub-3940256099942544~1458002511";
				 bannerID="ca-app-pub-3940256099942544/2934735716";
				 interstitialID="ca-app-pub-3940256099942544/4411468910";
				 videoID="ca-app-pub-3940256099942544/1712485313";
				 
#elif UNITY_ANDROID
        		 appID= "ca-app-pub-6590923007672971~2750910329";
				 bannerID= "ca-app-pub-6590923007672971/7664325652";
				 interstitialID= "ca-app-pub-6590923007672971/8288525071";
				// videoID="ca-app-pub-3940256099942544/5224354917";
				
#endif
        AdProperties adProperties = new AdProperties();
/*
        adProperties.isTesting(true);
        adProperties.isAppMuted(true);
        adProperties.isUnderAgeOfConsent(false);
        adProperties.appVolume(100);
        adProperties.maxAdContentRating(AdProperties.maxAdContentRating_G);
string[] keywords = { "key1", "key2", "key3" };
        adProperties.keyworks(keywords);
*/
        ad = Admob.Instance();
            ad.bannerEventHandler += onBannerEvent;
            ad.interstitialEventHandler += onInterstitialEvent;
        //    ad.rewardedVideoEventHandler += onRewardedVideoEvent;
          
            ad.initSDK(adProperties);//reqired,adProperties can been null
    }

    //Interstitial Ad
	public void ShowInterstitialAd() { 
           Debug.Log("Dummy Interstitial Ad -------------");
            if (ad.isInterstitialReady())
            {
                ad.showInterstitial();
            }
            else
            {
                ad.loadInterstitial(interstitialID);
            }
        }
    //Rewarded Ad
    public void ShowRewardedAd()
    {
        Debug.Log("Dummy Rewarded Ad -------------");
        if (ad.isRewardedVideoReady())
        {
            ad.showRewardedVideo();
        }
        else
        {
            ad.loadRewardedVideo(videoID);
        }
    }
    //Relative Banner Ad
    public void ShowRelativeBannerAd()
    {
        Admob.Instance().showBannerRelative(bannerID, AdSize.SMART_BANNER, AdPosition.BOTTOM_CENTER);
    }

    //Absolute Banner Ad
    public void ShowAbsoluteBannerAd()
    {
        Admob.Instance().showBannerAbsolute(bannerID, AdSize.BANNER, 20, 220, "multimax_banner_ad");
    }

    //Remove Banner Ad
    public void RemoveRelativeBannerAd()
    {
        Admob.Instance().removeBanner();
       
    }
    public void RemoveAbsoluteBannerAd()
    {
        Admob.Instance().removeBanner("multimax_banner_ad");
    }
      
	
    void onInterstitialEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
        if (eventName == AdmobEvent.onAdLoaded)
        {
            Admob.Instance().showInterstitial();
        }
    }
    void onBannerEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
    }
    void onRewardedVideoEvent(string eventName, string msg)
    {
        Debug.Log("handler onRewardedVideoEvent---" + eventName + "  rewarded: " + msg);
    }
    void onNativeBannerEvent(string eventName, string msg)
    {
        Debug.Log("handler onAdmobNativeBannerEvent---" + eventName + "   " + msg);
    }
}
